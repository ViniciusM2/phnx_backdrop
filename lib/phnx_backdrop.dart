library phnx_backdrop;

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PhnxBackdrop extends StatelessWidget {
  PhnxBackdrop({
    Key? key,
    required this.title,
    required this.frontLayer,
    required this.animationController,
    required this.onAffordanceClicked,
    this.leftWidget,
    this.rightWidget,
    this.isOpen = false,
    this.loading = false,
  }) : super(key: key);
  final VoidCallback onAffordanceClicked;
  final String title;
  final Widget frontLayer;
  final Widget? leftWidget;
  final Widget? rightWidget;
  final bool isOpen;
  final AnimationController animationController;
  final bool loading;
  final double _baseHeight = 65;
  final Duration _duration = Duration(milliseconds: 300);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Container(
          color: Get.theme.primaryColor,
        ),
        AnimatedPositioned(
          top: isOpen ? _baseHeight : _baseHeight * 2,
          bottom: 0,
          left: 0,
          right: 0,
          duration: _duration,
          child: Card(
            margin: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(0),
              ),
            ),
            child: frontLayer,
          ),
        ),
        AnimatedPositioned(
          top: isOpen ? 0 : _baseHeight,
          left: 0,
          right: 0,
          duration: _duration,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
            margin: EdgeInsets.zero,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(24, 12, 24, 12),
                  child: Row(
                    children: [
                      if (leftWidget != null)
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: leftWidget,
                        ),
                      Text(title),
                      Spacer(),
                      if (rightWidget is Widget) rightWidget!,
                      RotationTransition(
                        turns: Tween(begin: 0.0, end: 0.5)
                            .animate(animationController),
                        child: IconButton(
                          icon: Icon(Icons.expand_more),
                          onPressed: onAffordanceClicked,
                        ),
                      ),
                    ],
                  ),
                ),
                if (loading)
                  LinearProgressIndicator(
                    value: null,
                    backgroundColor: Get.theme.primaryColor.withOpacity(0.3),
                    color: Get.theme.primaryColor,
                  )
                else
                  const Divider(
                    height: 1,
                  ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
