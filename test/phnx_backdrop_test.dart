import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:phnx_backdrop/phnx_backdrop.dart';

void main() {
  group('PhnxBackdrop tests', () {
    testWidgets(
      "PhnxBackdrop shows title",
      (WidgetTester tester) async {
        const String title = 'Título de teste';

        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PhnxBackdrop(
                title: title,
                frontLayer: Container(),
                animationController:
                    AnimationController(vsync: const TestVSync()),
                onAffordanceClicked: () {},
              ),
            ),
          ),
        );

        final titleFinder = find.text(title);
        expect(titleFinder, findsOneWidget);
      },
    );

    testWidgets(
      "PhnxBackdrop is able to render ListTiles on a ListView",
      (WidgetTester tester) async {
        const String listTileTitle1 = 'Título 1';
        const String listTileTitle2 = 'Título 2';
        const String listTileSubtitle1 = 'Subtítulo 1';
        const String listTileSubtitle2 = 'Subtítulo 2';

        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: PhnxBackdrop(
                title: '',
                frontLayer: ListView(
                  children: const [
                    ListTile(
                      title: Text(listTileTitle1),
                      subtitle: Text(listTileSubtitle1),
                    ),
                    ListTile(
                      title: Text(listTileTitle2),
                      subtitle: Text(listTileSubtitle2),
                    ),
                  ],
                ),
                animationController:
                    AnimationController(vsync: const TestVSync()),
                onAffordanceClicked: () {},
              ),
            ),
          ),
        );

        final listTileTitle1Finder = find.text(listTileTitle1);
        final listTileTitle2Finder = find.text(listTileTitle2);
        final listTileSubtitle1Finder = find.text(listTileSubtitle1);
        final listTileSubtitle2Finder = find.text(listTileSubtitle2);

        expect(listTileTitle1Finder, findsOneWidget);
        expect(listTileTitle2Finder, findsOneWidget);
        expect(listTileSubtitle1Finder, findsOneWidget);
        expect(listTileSubtitle2Finder, findsOneWidget);
      },
    );
  });
}
